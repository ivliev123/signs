
import cv2
import numpy as np
import random
import matplotlib.pyplot as plt
import copy



def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
        for i in np.arange(0, 256)]).astype("uint8")

    # apply gamma correction using the lookup table
    return cv2.LUT(image, table)



left_turn1 = cv2.imread("/home/ivliev/tensorflow2/signs/left-turn2.png",1)
left_turn2 = cv2.imread("/home/ivliev/tensorflow2/signs/left-turn3.png",1)

right_turn1 = cv2.imread("/home/ivliev/tensorflow2/signs/right-turn1.png",1)
right_turn2 = cv2.imread("/home/ivliev/tensorflow2/signs/right-turn2.png",1)

break1 = cv2.imread("/home/ivliev/tensorflow2/signs/break1.png",1)
break2 = cv2.imread("/home/ivliev/tensorflow2/signs/break2.png",1)
break3 = cv2.imread("/home/ivliev/tensorflow2/signs/break3.png",1)

signs_array = []
signs_array.append([left_turn1,"left_turn"])
signs_array.append([left_turn2,"left_turn"])
signs_array.append([right_turn1,"right_turn"])
signs_array.append([right_turn2,"right_turn"])
signs_array.append([break1,"break"])
signs_array.append([break2,"break"])
signs_array.append([break3,"break"])


img_file = open('annotations/trainval.txt', 'w')



# quantity artifical data
quantity = 300
for i in range(quantity):
    # random
    fone = cv2.imread("fone.jpeg",cv2.IMREAD_UNCHANGED)
    gama = random.uniform(0.3,4)
    fone = adjust_gamma(fone,gama)

    info_about_signs = []

    quantity_signs_on_img = random.randint(1,5)
    for k in range(quantity_signs_on_img):
        num_signs_from_array = random.randint(0,6)

        img = copy.deepcopy(signs_array[num_signs_from_array][0])

        rows,cols,ch = img.shape

        w,h,z = img.shape


        # random
        turn = random.randint(0,1)
        if(turn):
            left_transform = random.uniform(-0.2,-1)
        else:
            left_transform = random.uniform(-0.2,-1)

        if (left_transform < 0):
            right_transform = -left_transform
            left_transform = 1
        else:
            right_transform = 1



        pts1 = np.float32([[0,0],[w,0],[w,h],[0,h]])
        pts2 = np.float32([[(w-(w*left_transform*right_transform))/2,h*((1-left_transform)/4)],
                            [w-(w-(w*left_transform*right_transform))/2,h*((1-right_transform)/4)],
                            [w-(w-(w*left_transform*right_transform))/2,h*(1-(1-right_transform)/4)],
                            [(w-(w*left_transform*right_transform))/2,h*(1-(1-left_transform)/2)]])

        M = cv2.getPerspectiveTransform(pts1,pts2)
        dst = cv2.warpPerspective(img,M,(w,h))

        # random
        size = random.randint(40, 100)
        dst = cv2.resize(dst,(size,size))

        imgray = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(imgray, 10, 255, 0)
        cv2.imwrite('thresh.png',thresh)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)


        c = sorted(contours, key = cv2.contourArea, reverse = True)

        x,y,w,h = cv2.boundingRect(c[0])
        # dst = dst[y:y+h, x:x+w]

        # random
        gama = random.uniform(0.3,4)
        dst = adjust_gamma(dst,gama)

        # random
        x_pix=random.randint(size,fone.shape[1]-size)
        y_pix=random.randint(size,fone.shape[0]-size)

        # cv2.rectangle(fone,(x_pix+x,y_pix+y),(x_pix+x+w,y_pix+y+h),(0,255,0),2)

        info_about_signs.append([signs_array[num_signs_from_array][1], x_pix+x, y_pix+y, x_pix+x+w, y_pix+y+h ])

        for z in range(dst.shape[2]):
            for y in range(dst.shape[0]):
                for x in range(dst.shape[1]):
                    if(thresh[y][x] !=0):
                        fone[y+y_pix][x+x_pix][z] = dst[y][x][z]


    img_save = 'fone_new'+str(i)+'.jpg'
    cv2.imwrite('images2/test/fone_new'+str(i)+'.jpg',fone)
    # print(info_about_signs)
    # print()

    f = open('images2/test/fone_new'+str(i)+'.xml', 'w')

    height = fone.shape[0]
    width = fone.shape[1]

    f.write("<annotation>\n")
    f.write("    <folder>/home/ivliev/tensorflow2/image2/test</folder>\n")
    f.write("    <filename>"+img_save+"</filename>\n")
    f.write("    <size>\n")
    f.write("        <width>"+str(width)+"</width>\n")
    f.write("        <height>"+str(height)+"</height>\n")
    f.write("   </size>\n")
    f.write("    <segmented>0</segmented>\n")


    for n in range(len(info_about_signs)):
        f.write("    <object>\n")
        f.write("        <name>"+info_about_signs[n][0]+"</name>\n")
        f.write("        <bndbox>\n")
        f.write("            <xmin>"+str(info_about_signs[n][1])+"</xmin>\n")
        f.write("            <ymin>"+str(info_about_signs[n][2])+"</ymin>\n")
        f.write("            <xmax>"+str(info_about_signs[n][3])+"</xmax>\n")
        f.write("            <ymax>"+str(info_about_signs[n][4])+"</ymax>\n")
        f.write("        </bndbox>\n")
        f.write("    </object>\n")


    f.write("</annotation>\n")




    f.close()


    img_file.write('fone_new'+str(i) + '\n')

img_file.close()
