from __future__ import print_function
import cv2 as cv
import numpy as np
import argparse


src = cv.imread("/home/ivliev/tensorflow2/signs/left-turn3.png",1)
if src is None:
    print('Could not open or find the image:', args.input)
    exit(0)
srcTri = np.array( [[0, 0], [src.shape[1] , 0], [src.shape[1], src.shape[0]], [0, src.shape[0]]] ).astype(np.float32)

dstTri = np.array( [[0, 0], [src.shape[1] , 0], [src.shape[1], src.shape[0]], [0, src.shape[0]]] ).astype(np.float32)
warp_mat = cv.getAffineTransform(srcTri, dstTri)
warp_dst = cv.warpAffine(src, warp_mat, (src.shape[1], src.shape[0]))
# Rotating the image after Warp
center = (src.shape[1]//2, src.shape[0]//2)
angle = -50
scale = 1
rot_mat = cv.getRotationMatrix2D( center, angle, scale )
warp_rotate_dst = cv.warpAffine(src, rot_mat, (src.shape[1], src.shape[0]))
cv.imshow('Source image', src)
cv.imshow('Warp', warp_dst)
# cv.imshow('Warp + Rotate', warp_rotate_dst)
cv.waitKey()
